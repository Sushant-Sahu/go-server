package main

import (
    "fmt"
    "net/http"
    "io"
)

func helloHandler(w http.ResponseWriter, r *http.Request) {
    
    //v:=r.FormValue("name")
    //fmt.Fprintf(w, "Hello! Welcome "+v)

    fmt.Fprintf(w, "Hello!\n")

    key := "name"
    val := r.URL.Query().Get(key)
    io.WriteString(w, "Welcome " +val)
}


func main() {

    fmt.Printf("Starting server at port 8080\n")
    http.HandleFunc("/", helloHandler)
    http.ListenAndServe(":8080", nil)
    
}
